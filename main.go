package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
)

var help = flag.String("help", "启动进程2019年7月4日", "输出帮助信息")

func main() {

	flag.Parse()

	fmt.Println(os.Args)
	fmt.Println(len(os.Args))
	if len(os.Args) == 1 {
		fmt.Println("请输入参数")
		return
	}
	args := os.Args[2:]
	fmt.Println(args)
	cmd := exec.Command(os.Args[1], args...)
	fmt.Println(os.Args[1])

	err := cmd.Start()
	if err != nil {
		fmt.Println("err")
	}
	fmt.Println("gostart is running")
}
